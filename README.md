# Code Quality report in merge request

## Code Quality Widget

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1984) in GitLab 9.3.
> - Made [available in all tiers](https://gitlab.com/gitlab-org/gitlab/-/issues/212499) in 13.2.

This project demonstrates how to implement code quality reports with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/README.html).

You can configure your job to use Code quality reports, and GitLab will analyze your source code quality and display a report on the merge request if a report from the target branch is available to compare to/

<img src="https://docs.gitlab.com/ee/user/project/merge_requests/img/code_quality_widget_13_11.png" alt="A merge request with code quality widget" align="center">

## Code Quality in diff view

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/267612) in GitLab Ultimate 13.11.
> - [Enabled by default](https://gitlab.com/gitlab-org/gitlab/-/issues/284140) in GitLab 13.12.
> - [Feature enhanced](https://gitlab.com/gitlab-org/gitlab/-/issues/2526) in GitLab 14.0. 

Changes to files in merge requests can cause Code Quality to fall if merged. In these cases, the merge request’s diff view displays an indicator next to lines with new Code Quality violations. For example: 

<img src="https://docs.gitlab.com/ee/user/project/merge_requests/img/code_quality_mr_diff_report_v14.png" alt="A merge request with code quality in diff view" align="center">

Previously, an indicator was displayed ( Code Quality) on the file in the merge request’s diff view: 

<img src="https://docs.gitlab.com/ee/user/project/merge_requests/img/code_quality_mr_diff_report_v13_11.png" alt="A merge request with code quality in diff view" align="center">

## Demo

This demo implementation uses Ruby.

There is 1 sample merge requests that shows both features in action:

- [Merge request #1](https://gitlab.com/gitlab-org/ci-sample-projects/codequality-report/-/merge_requests/1) shows a code submission **with** new quality degradations.

---

Read more about the feature on [GitLab documentation](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html).
